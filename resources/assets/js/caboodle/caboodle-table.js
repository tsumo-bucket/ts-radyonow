/*!
 * Caboodle Table
 * http://sumofy.me/
 *
 * Copyright Think Sumo Creative Media Inc.
 * Developed by Developed by Jiez Lawsin
 * Released under the MIT license.
 * http://sumofy.me/
 *
 * Modified by Biboy Oyales
 */

const MDCCheckbox = mdc.checkbox.MDCCheckbox;
const MDCCheckboxFoundation = mdc.checkbox.MDCCheckboxFoundation;

$(document).ready(function() {
	$('.caboodle-table-col-action').addClass('hide');
    $('.caboodle-table-col-header').removeClass('hide');

    if($("tr.collapse-trigger").length > 0) {
        if($("body").find(".side-collapse-controller").length == 0) {
            $("body").append(`<div class="side-collapse-controller"><div class="controls-container"></div></div>`);
        }
    }
    
    $(document)
        .on('click', 'tr.collapse-trigger, .side-collapse-controller .controls-container .collapse-control .collapse-btn', function(e) {
            if(e.target.className.indexOf('mdc-checkbox__native-control') == -1 && $(e.target).attr('href') == undefined) {
                var collapseContainer = $("tr.collapse-" + $(this).data('id') + " .collapse-container");
                var isCollapse = $('tr.collapse-' + $(this).data('id')).hasClass('in');
                var maxHeight = isCollapse ? 0 : collapseContainer.find('.collapse-control-container').outerHeight();

                $('tr.collapse-' + $(this).data('id')).toggleClass('in');
                $(`.collapse-control[data-id=${$(this).data('id')}]`).toggleClass('in');
                $("tr.collapse-" + $(this).data('id') + " .collapse-container").animate({
                    'max-height': maxHeight
                }, 500);
            }
        })
        .on("click", ".side-collapse-controller .controls-container .collapse-control .location-btn", function() {
            var position = $(`tr.collapse-trigger[data-id=${$(this).data("id")}]`).offset().top - $(".navbar").outerHeight();
            $("body, html").animate({
                scrollTop: position
            }, 500);
        });

    $.each($("tr.collapse-trigger"), function(i, v) {
        $(".side-collapse-controller .controls-container")
            .append(
                `<div class="collapse-control flex align-center" data-id="${$(this).data("id")}">
                    <button
                        class="location-btn" data-id="${$(this).data("id")}"
                        data-toggle="tooltip"
                        title="Go to item ${i + 1}."
                    >
                        <i class="fal fa-location-arrow"></i>
                    </button>
                    <button 
                        data-toggle="tooltip"
                        title="Collapse item ${i + 1}."
                        class="collapse-btn"
                        data-id="${$(this).data("id")}"
                    >
                        <i class="fal fa-minus-square"></i>
                    </button>
                </div>`
            );
    });
});

$('body').on('change', '.caboodle-table [type="checkbox"]:not(.row-data-control)', function(e) {
    var rowEl = $(this).closest('tr');
    if (rowEl.hasClass('depth-parent')) {
        var key = rowEl.attr('key');
        var parentDepth = $('.caboodle-table .depth-parent[key="' + key + '"]');
        var parentCheckbox = parentDepth.find('[type="checkbox"]');
        var parentMDC = parentDepth.find('.mdc-checkbox');
        var parentMDCCheckbox = new MDCCheckbox(parentMDC[0]);
        var childCheckboxes = $('.caboodle-table .depth-child[parent="' + key + '"] [type="checkbox"]');
        var childCheckboxes_enabled = $('.caboodle-table .depth-child[parent="' + key + '"] [type="checkbox"]:checked');

        if (childCheckboxes.length == childCheckboxes_enabled.length) {
            var hasDisabled = false;
            childCheckboxes.each(function (e) {
                if (!$(this).attr('disabled')) {
                    $(this).prop('checked', false);
                } else {
                    hasDisabled = true;
                }
            });
            if (hasDisabled) {
                // console.log('hasDisablwed');
                parentCheckbox.prop('checked', false);
                parentMDCCheckbox.indeterminate = true;
                parentMDCCheckbox.checked = false;
            }
        } else {
            childCheckboxes.each(function (e) {
                $(this).prop('checked', true);
            });
        }
    }

    if (rowEl.hasClass('depth-child')) {
        var parentKey = rowEl.attr('parent');
        var parentDepth = $('.caboodle-table .depth-parent[key="' + parentKey + '"]');
        var parentCheckbox = parentDepth.find('[type="checkbox"]');
        var parentMDC = parentDepth.find('.mdc-checkbox');
        var parentMDCCheckbox = new MDCCheckbox(parentMDC[0]);
        var childCheckboxes = $('.caboodle-table .depth-child[parent="' + parentKey + '"] [type="checkbox"]');
        var childCheckboxes_enabled = $('.caboodle-table .depth-child[parent="' + parentKey + '"] [type="checkbox"]:checked');

        if (childCheckboxes.length == childCheckboxes_enabled.length) {
            parentCheckbox.prop('checked', true);
            parentMDCCheckbox.checked = true;
            parentMDCCheckbox.indeterminate = false;
        } else {
            parentCheckbox.prop('checked', false);
            if (childCheckboxes_enabled.length > 0) {
                parentMDCCheckbox.indeterminate = true;
                parentMDCCheckbox.checked = false;
            } else {
                parentMDCCheckbox.checked = false;
                parentMDCCheckbox.indeterminate = false;
            }
        }
    }

	var parentTable = $(this).closest('.caboodle-table');
	var checkboxes = parentTable.find('[type="checkbox"]:not(.row-data-control)');
    var checkboxes_enabled = parentTable.find('[type="checkbox"]:not(.row-data-control):checked');
    var select_all = parentTable.find('.caboodle-table-select-all');
    
    if (select_all.length > 0) {
        var checkboxSelectAll = new MDCCheckbox(select_all[0]);
        var target = $(this).attr('name');
        
        if (target == 'select_all') {
            checkboxes.each(function(e) {
                if (!$(this).attr('disabled')) {
                    $(this).prop('checked', checkboxSelectAll.checked);
                    var thisCheckboxMDC = $(this).closest('.mdc-checkbox');
                    var thisMDCCheckbox = new MDCCheckbox(thisCheckboxMDC[0]);
                    thisMDCCheckbox.checked = checkboxSelectAll.checked;
                    thisMDCCheckbox.indeterminate = false;
                }
                caboodleTableActionsToggle(parentTable, checkboxSelectAll.checked);
            });
        } else {
            if (checkboxes.length == checkboxes_enabled.length) {
                checkboxSelectAll.checked = true;
                checkboxSelectAll.indeterminate = false;
                caboodleTableActionsToggle(parentTable, true);
            } else {
                if (checkboxes_enabled.length > 0) {
                    checkboxSelectAll.indeterminate = true;
                    checkboxSelectAll.checked = false;
                    caboodleTableActionsToggle(parentTable, true);
                } else {
                    caboodleTableActionsToggle(parentTable, false);
                    checkboxSelectAll.checked = false;
                    checkboxSelectAll.indeterminate = false;
                }
            }
        }
    }
});

function caboodleTableActionsToggle(parentTable, status) {
    parentTable.find('.caboodle-table-col-action').toggleClass('hide', !status);
    parentTable.find('.caboodle-table-col-header').toggleClass('hide', status);
}
