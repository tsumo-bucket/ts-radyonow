{!! Form::model($seo, ['class'=>'form form-seo']) !!}
<div class="caboodle-form-group">
  <label for="name">Title</label>
  {!! Form::text('title', null, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>'Title']) !!}
  {!! Form::hidden('seoable_id', $data->id) !!}
  {!! Form::hidden('seoable_type', get_class($data)) !!}
</div>
<div class="caboodle-form-group">
  <label for="evaluation">Description</label>
  {!! Form::textarea('description', null, ['class'=>'form-control', 'id'=>'description', 'placeholder'=>'Description']) !!}
</div>
<div class="caboodle-form-group sumo-asset-select">
  <label for="image">Image</label>
  {!! Form::hidden('image', null, ['class'=>'sumo-asset']) !!}
  <span class="sub-text-1">The required image size is 100x100 pixels minimum</span>
</div>
<div class="caboodle-form-group clearfix">
  <button type="submit" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-seo-submit float-right" data-mdc-auto-init="MDCRipple">
    <i class="fa fa-check" aria-hidden="true"></i>
    Save
  </button>
</div>
{!! Form::close() !!}